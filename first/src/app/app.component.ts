import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sensorId = 100;
  sensorStatus = 'Выключен';
  allowNewSensor = false;
  sensorCreationStatus = 'Ничего не было добавлено';
  sensorSn = '000-000-000';
  SNs = [];

  constructor() {
    setTimeout(() => this.allowNewSensor = true, 2000);
  }

  onCreateSensor() {
    this.sensorCreationStatus = 'Добавлен датчик с номером ' + this.sensorSn;
    this.SNs.push(this.sensorSn);
  }
}
